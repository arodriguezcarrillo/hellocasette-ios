//
//  Artist.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 10/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "Artist.h"

@implementation Artist

-(id)initWithId:(NSNumber *)ident andName:(NSString *)name andImage:(NSString *)image andYear:(NSNumber*)year{
    self = [super init];
    
    if (self){
        _ident = ident;
        _name = name;
        _image = image;
        _year = year;
    }
    
    return self;
}

-(id)initWithId:(NSNumber *)ident andName:(NSString *)name andImage:(NSString *)image andYear:(NSNumber*)year andBiography:(NSString *)biography{
    self = [super init];
    
    if (self){
        _ident = ident;
        _name = name;
        _image = image;
        _biography = biography;
        _year = year;
    }
    
    return self;
}

+(id)artistWithId:(NSNumber *)ident andName:(NSString *)name andImage:(NSString *)image andYear:(NSNumber*)year{
    return [[Artist alloc] initWithId:ident andName:name andImage:image andYear:year];
}

+(id)artistWithId:(NSNumber *)ident andName:(NSString *)name andImage:(NSString *)image andYear:(NSNumber*)year andBiography:(NSString *)biography{
    return [[Artist alloc] initWithId:ident andName:name andImage:image andYear:year andBiography:biography];
}

-(NSString *)description{
    return [NSString stringWithFormat:@"%@ - %@ - %@", _ident, _name, _year];
}

@end
