//
//  Album.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 15/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Album : NSObject

@property NSNumber* ident;
@property NSString* name;
@property NSString *image;
@property NSNumber* year;
@property NSNumber* artist;

-(id)initForArtist:(NSNumber*)artist withIdent:(NSNumber*)ident andName:(NSString*)name andImage:(NSString*)image andYear:(NSNumber*)year;
+(id)albumForArtist:(NSNumber*)artist withIdent:(NSNumber*)ident andName:(NSString*)name andImage:(NSString*)image andYear:(NSNumber*)year;

@end
