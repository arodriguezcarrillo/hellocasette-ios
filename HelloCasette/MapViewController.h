//
//  MapViewController.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 1/3/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController<CLLocationManagerDelegate, UIAlertViewDelegate>

@end
