//
//  MKPoint.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 1/3/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MKPoint : NSObject<MKAnnotation>


@end
