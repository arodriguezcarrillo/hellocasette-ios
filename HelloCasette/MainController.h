//
//  MainController.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 24/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicViewController.h"
#import "MapViewController.h"

@interface MainController : UIViewController<UITableViewDataSource, UITableViewDelegate,
                MusicViewControllerDelegate>

@end
