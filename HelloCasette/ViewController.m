//
//  ViewController.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 10/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "ViewController.h"
#import "ArtistDB.h"


@interface ViewController ()
@property (weak) IBOutlet UITextField *txtEmail;
@property (weak) IBOutlet UITextField *txtPassword;
@end

@implementation ViewController{
    MusicBO *_businessObjects;
    UITextField *activeTextField;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
}

-(IBAction)btnLoginClick{
    NSString *username = _txtEmail.text;
    NSString *password = _txtPassword.text;
    
    [_businessObjects UserLoginWithEmail:username AndPassword:password];
}

-(void)dismissKeyboard{
    if (activeTextField != nil){
        [activeTextField resignFirstResponder];
        activeTextField = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _businessObjects = [[MusicBO alloc] initWithDelegate:self];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [self performSegueWithIdentifier:@"loginSegue" sender:self];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)keyboardWillShow{
    [self moveViewToUp:YES];
}

-(void)keyboardWillHide{
    [self moveViewToUp:NO];
}

-(void)moveViewToUp:(BOOL)up{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    
    CGRect rect = self.view.frame;
    NSInteger moveTo = 0;
    if (up && activeTextField != nil){
        moveTo = activeTextField.frame.origin.y -
                 (self.view.frame.size.height - 300);
        
        if (moveTo < 0)
            moveTo = 0;
    }
    rect.origin.y = -moveTo;
    
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    BOOL result = YES;
    
    if ([identifier isEqualToString:@"loginSegue"]){
        result = [_businessObjects isUserLoggedIn];
    }
    
    return result;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MusicBO Delegate

-(void)userLogged{
    [self performSegueWithIdentifier:@"loginSegue" sender:self];
}


@end
