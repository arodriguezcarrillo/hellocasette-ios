//
//  ViewController.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 10/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MusicBO.h"

@interface ViewController : UIViewController<UITextFieldDelegate, MusicBODelegate>


@end

