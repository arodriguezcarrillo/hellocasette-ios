//
//  AlbumDB.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 26/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "AlbumDB.h"
#import "ArtistDB.h"


@implementation AlbumDB

@dynamic ident;
@dynamic name;
@dynamic artist;

@end
