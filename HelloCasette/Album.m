//
//  Album.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 15/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "Album.h"

@implementation Album

-(id)initForArtist:(NSNumber *)artist withIdent:(NSNumber *)ident andName:(NSString *)name andImage:(NSString *)image andYear:(NSNumber *)year{
    self = [super init];
    
    if (self){
        _artist = artist;
        _ident = ident;
        _name = name;
        _year = year;
    }
    
    return self;
}

+(id)albumForArtist:(NSNumber *)artist withIdent:(NSNumber *)ident andName:(NSString *)name andImage:(NSString *)image andYear:(NSNumber *)year{
    return [[Album alloc] initForArtist:artist withIdent:ident andName:name andImage:image andYear:year];
}

-(NSString *)description{
    return [NSString stringWithFormat:@"%@ - %@ - %@", _ident, _name, _year];
}

@end
