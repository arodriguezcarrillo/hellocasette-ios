//
//  MapViewController.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 1/3/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>


@interface MapViewController ()

@property (weak) IBOutlet MKMapView *mapview;

@end

@implementation MapViewController{
    CLLocationManager *locationManager;
    BOOL markersetted;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    [locationManager requestWhenInUseAuthorization];
    [locationManager startUpdatingLocation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CLLocationManager Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    NSLog(@"%@", [locations lastObject]);
    [self setMarkerWithLocation:[locations lastObject]];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    UIAlertView *alertview = [[UIAlertView alloc] initWithTitle:@"Atención" message:@"Se ha producido un error cargando los datos de la localización" delegate:self cancelButtonTitle:@"Cancelar" otherButtonTitles:@"OK", nil];
    
    NSLog(@"%@", error);
    [alertview show];
}

-(void)setMarkerWithLocation:(CLLocation*)location{
    if (!markersetted){
        //_mapView addAnnotation:
    }
    
    markersetted = YES;
}


#pragma mark - UIAlertView delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"%@",[alertView buttonTitleAtIndex:buttonIndex]);
}

@end
