//
//  MyRestTask.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 24/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>

enum CallType {
    GET = 0,
    POST = 1
};

@interface MyRestTask : NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate>



@end
