//
//  MusicViewController.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 24/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "MusicViewController.h"
#import "Artist.h"

@interface MusicViewController ()

@end

@implementation MusicViewController{
    NSArray *_artists;
    MusicBO *_businessObjects;
}

-(id)initWithDelegate:(id<MusicViewControllerDelegate>)delegate{
    self = [self initWithNibName:@"MusicViewController" bundle:nil];
    
    if (self){
        self.delegate = delegate;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _businessObjects = [[MusicBO alloc] initWithDelegate:self];
    [_businessObjects ArtistsBySearch:@""];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"ArtistCell" bundle:nil] forCellReuseIdentifier:@"ArtistCell"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Music BO Callback

-(void)artistCompleted:(NSArray *)artists{
    _artists = artists;
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_artists count];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ArtistCell" forIndexPath:indexPath];
    
    UILabel *label = (UILabel*)[cell viewWithTag:0];
    
    Artist *artist = [_artists objectAtIndex:indexPath.row];
    [label setText:artist.name];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Artist* artist = [_artists objectAtIndex:indexPath.row];
    
    if (self.delegate != nil)
        [self.delegate ArtistClicked:artist];
}

-(void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"BeginEditing");
}

-(NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Deselect");
    
    return indexPath;
}

@end
