//
//  MusicBO.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 25/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServiceCall.h"
#import "ArtistDB.h"
#import "Artist.h"

@protocol MusicBODelegate <NSObject>

@optional
-(void)userLogged;
-(void)artistCompleted:(NSArray*)artists;
-(void)albumsCompleted:(NSArray*)albums;
-(void)songsCompleted:(NSArray*)songs;

@end

@interface MusicBO : NSObject<ServiceCallDelegate>

-(id)initWithDelegate:(id<MusicBODelegate>)delegate;
@property (weak) id<MusicBODelegate> delegate;

-(BOOL)isUserLoggedIn;
-(void)UserLoginWithEmail:(NSString*)email AndPassword:(NSString*)password;
-(void)RegisterUserWithName:(NSString*)name AndSurname:(NSString*)surname AndEmail:(NSString*)email
                  AndRemail:(NSString*)remail AndPassword:(NSString*)password AndRepassword:(NSString*)repassword
                     AndAge:(NSNumber*)number;


-(void)ArtistsBySearch:(NSString*)search;
-(void)AlbumsByArtist:(NSNumber*)artist;
-(void)SongsByAlbum:(NSNumber*)album;



-(void)InsertArtist:(ArtistDB*)artist;

@end
