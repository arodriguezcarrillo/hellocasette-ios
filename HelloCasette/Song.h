//
//  Song.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 15/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Song : NSObject

@property NSNumber* ident;
@property NSString* name;
@property NSNumber* duration;
@property NSString* quality;
@property NSNumber* number;
@property NSNumber* album;
@property NSString* uri;

-(id)initForAlbum:(NSNumber*)album withIdent:(NSNumber*)ident andName:(NSString*)name andDuration:(NSNumber*)duration andQuality:(NSString*)quality andNumber:(NSNumber*)number;
+(id)songForAlbum:(NSNumber*)album withIdent:(NSNumber*)ident andName:(NSString*)name andDuration:(NSNumber*)duration andQuality:(NSString*)quality andNumber:(NSNumber*)number;

@end
