//
//  MusicViewController.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 24/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Artist.h"
#import "MusicBO.h"

@protocol MusicViewControllerDelegate

-(void)ArtistClicked:(Artist*)artist;

@end

@interface MusicViewController : UITableViewController<MusicBODelegate>

-(id)initWithDelegate:(id<MusicViewControllerDelegate>)delegate;
@property (weak) id<MusicViewControllerDelegate> delegate;

@end
