//
//  MusicBO.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 25/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "MusicBO.h"
#import "AppDelegate.h"
#import "Album.h"
#import "Song.h"

enum MusicCalls{
    MusicCallLogin = 1,
    MusicCallRegister = 2,
    MusicCallArtists = 3,
    MusicCallAlbums = 4,
    MusicCallSongs = 5
};

@implementation MusicBO

-(id)initWithDelegate:(id<MusicBODelegate>)delegate{
    self = [super init];
    
    if (self)
        self.delegate = delegate;
    
    return self;
}

#pragma mark - Service Methods

-(void)UserLoginWithEmail:(NSString *)email AndPassword:(NSString *)password{
    ServiceCall *call = [[ServiceCall alloc] initWithDelegate:self andId:MusicCallLogin];
    [call makeRequestWithURL:@"login.json"
        andParameters:@{
            @"user":email,
            @"passwd":password}
        andType:POST];
}

-(void)RegisterUserWithName:(NSString *)name AndSurname:(NSString *)surname AndEmail:(NSString *)email AndRemail:(NSString *)remail AndPassword:(NSString *)password AndRepassword:(NSString *)repassword AndAge:(NSNumber *)age{
    ServiceCall *call = [[ServiceCall alloc] initWithDelegate:self andId:MusicCallRegister];
    //TODO: Hacer la comprobación de contraseñas y correos electrónicos
    [call makeRequestWithURL:@"users/user.json"
               andParameters:@{
                               @"name":name,
                               @"surname":surname,
                               @"email":email,
                               @"passwd":password,
                               @"age":age}
                     andType:POST];
}

-(BOOL)isUserLoggedIn{
    return [self GetUserToken] != nil;
}

-(void)ArtistsBySearch:(NSString *)search{
    ServiceCall *call = [[ServiceCall alloc] initWithDelegate:self andId:MusicCallArtists];
    //TODO: Hacer la comprobación de contraseñas y correos electrónicos
    [call makeRequestWithURL:@"artists/all.json"
               andParameters:@{
                               @"search":search,
                               @"token":[self GetUserToken]
                            }
                     andType:GET];
}

-(void)AlbumsByArtist:(NSNumber*)artist{
    ServiceCall *call = [[ServiceCall alloc] initWithDelegate:self andId:MusicCallAlbums];
    //TODO: Hacer la comprobación de contraseñas y correos electrónicos
    [call makeRequestWithURL:@"albums/all.json"
               andParameters:@{
                               @"artist":artist,
                               @"token":[self GetUserToken]
                               }
                     andType:GET];
}

-(void)SongsByAlbum:(NSNumber *)album{
    ServiceCall *call = [[ServiceCall alloc] initWithDelegate:self andId:MusicCallSongs];
    //TODO: Hacer la comprobación de contraseñas y correos electrónicos
    [call makeRequestWithURL:@"songs/all.json"
               andParameters:@{
                               @"album":album,
                               @"token":[self GetUserToken]
                               }
                     andType:GET];
}

#pragma mark - User Token

-(NSString*)GetUserToken{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    return [userDefaults objectForKey:@"token"];
}

-(void)SaveUserToken:(NSString*)token{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [userDefaults setValue:token forKey:@"token"];
    
}

#pragma mark - Service Call Delegate

-(void)ServiceCallSuccededWithData:(NSData *)data andId:(NSInteger)ident{
    NSString *decodedData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"%@", decodedData);
    
    
    @try{
        NSError * error;
        id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
        
        if (ident == MusicCallRegister || ident == MusicCallLogin){
            NSDictionary *data = [jsonData objectForKey:@"data"];
            
            [self SaveUserToken:[data objectForKey:@"token"]];
            
            NSLog(@"%@", data);
        } else if (ident == MusicCallArtists){
            NSArray *data = [jsonData objectForKey:@"data"];
            [self ProcessArtistsData:data];
        } else if (ident == MusicCallAlbums){
            NSArray *data = [jsonData objectForKey:@"data"];
            [self ProcessAlbumsData:data];
        } else if (ident == MusicCallSongs){
            NSArray *data = [jsonData objectForKey:@"data"];
            [self ProcessSongsData:data];
        }
    }
    @catch (NSException *excep){
        NSLog(@"%@", excep.description);
    }
    @finally{
        
    }
}

-(void)ProcessArtistsData:(NSArray*)data{
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    for (NSDictionary* object in data) {
        Artist *item = [[Artist alloc] init];
        item.ident = [object objectForKey:@"id"];
        item.name = [object objectForKey:@"name"];
        item.image = [object objectForKey:@"image"];
        item.year = [object objectForKey:@"year"];
        
        [items addObject:item];
    }
    
    [self.delegate artistCompleted:items];
}

-(void)ProcessAlbumsData:(NSArray*)data{
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    for (NSDictionary* object in data) {
        Album *item = [[Album alloc] init];
        item.ident = [object objectForKey:@"id"];
        item.name = [object objectForKey:@"name"];
        item.image = [object objectForKey:@"image"];
        item.year = [object objectForKey:@"year"];
        
        [items addObject:item];
    }
    
    [self.delegate albumsCompleted:items];
}

-(void)ProcessSongsData:(NSArray*)data{
    NSMutableArray *items = [[NSMutableArray alloc] init];
    
    for (NSDictionary* object in data) {
        Song *item = [[Song alloc] init];
        item.ident = [object objectForKey:@"id"];
        item.name = [object objectForKey:@"name"];
        item.uri = [object objectForKey:@"uri"];
        
        [items addObject:item];
    }
    
    [self.delegate songsCompleted:items];
}

-(void)ServiceCallErrorWithData:(NSString *)error andId:(NSInteger)ident{
    //TODO: Manejar errores
    
}

#pragma mark - Database Actions

-(NSManagedObjectContext*)ObjectContext{
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    return [delegate managedObjectContext];
}

/*
-(void)ArtistBySearch:(NSString *)search{
    NSManagedObjectContext *context = [self ObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@",search];

    [request
        setEntity:[NSEntityDescription
                   entityForName:@"ArtistDB"
                   inManagedObjectContext:context]];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *results = [context executeFetchRequest:request error:&error];
    
    [self.delegate artistCompleted:results];
}
 */

-(void)InsertArtist:(ArtistDB*)artist{
    NSManagedObjectContext *context = [self ObjectContext];
    
    [context insertObject:artist];
    
    NSError* error;
    [context save:&error];
}

-(void)SaveChanges{
    NSManagedObjectContext *context = [self ObjectContext];
    
    NSError* error;
    [context save:&error];

}

-(void)DeleteArtist:(ArtistDB*)artist{
    NSManagedObjectContext *context = [self ObjectContext];
    
    [context deleteObject:artist];
    
    NSError* error;
    [context save:&error];
}

@end
