//
//  RegisterViewController.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 16/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate>

@end
