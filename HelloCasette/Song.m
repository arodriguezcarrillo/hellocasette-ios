//
//  Song.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 15/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "Song.h"

@implementation Song

-(id)initForAlbum:(NSNumber *)album withIdent:(NSNumber *)ident andName:(NSString *)name andDuration:(NSNumber *)duration andQuality:(NSString *)quality andNumber:(NSNumber *)number{
    self = [super init];
    
    if (self){
        _album = album;
        _ident = ident;
        _name = name;
        _duration = duration;
        _quality = quality;
        _number = number;
    }
    
    return self;
}

+(id)songForAlbum:(NSNumber *)album withIdent:(NSNumber *)ident andName:(NSString *)name andDuration:(NSNumber *)duration andQuality:(NSString *)quality andNumber:(NSNumber *)number{
    return [[Song alloc] initForAlbum:album withIdent:ident andName:name andDuration:duration andQuality:quality andNumber:number];
}

-(NSString *)description{
    return [NSString stringWithFormat:@"%@ - %@ - %@", _ident, _name, _duration];
}


@end
