//
//  RegisterViewController.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 16/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController{
    UITextField *activeTextField;
}

-(IBAction)btnBackClick:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *recognizer =
        [[UITapGestureRecognizer alloc]
         initWithTarget:self
         action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:recognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard{
    if (activeTextField != nil)
        [activeTextField resignFirstResponder];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    activeTextField = textField;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
