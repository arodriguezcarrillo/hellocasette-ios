//
//  ArtistDB.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 26/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface ArtistDB : NSManagedObject

@property (nonatomic, retain) NSNumber * ident;
@property (nonatomic, retain) NSString * name;

@end
