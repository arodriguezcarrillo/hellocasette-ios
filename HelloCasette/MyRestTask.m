//
//  MyRestTask.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 24/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "MyRestTask.h"

@implementation MyRestTask{
    NSMutableData *mutableData;
}

-(void)MakeRequest{
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://www.google.es"]];
    
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    [conn start];
    
    if (GET == self.calltype){
        
    }
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [mutableData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    mutableData = [[NSMutableData alloc] init];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
}

@end
