//
//  Artist.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 10/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Artist : NSObject

@property NSNumber *ident;
@property NSString *name;
@property NSString *image;
@property NSString *biography;
@property NSNumber *year;

-(id)initWithId:(NSNumber*)ident andName:(NSString*)name andImage:(NSString*)image andYear:(NSNumber*)year andBiography:(NSString*)biography;
-(id)initWithId:(NSNumber*)ident andName:(NSString*)name andImage:(NSString*)image andYear:(NSNumber*)year;
+(id)artistWithId:(NSNumber*)ident andName:(NSString*)name andImage:(NSString*)image andYear:(NSNumber*)year andBiography:(NSString*)biography;
+(id)artistWithId:(NSNumber*)ident andName:(NSString*)name andImage:(NSString*)image andYear:(NSNumber*)year;

@end
