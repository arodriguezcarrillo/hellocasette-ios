//
//  ServiceCall.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 25/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "ServiceCall.h"

const NSString *BASEURL = @"http://vps111149.ovh.net/hellocasette/service/";

@implementation ServiceCall{
    NSMutableData *_data;
    NSInteger _ident;
}

-(id)initWithDelegate:(id<ServiceCallDelegate>)delegate andId:(NSInteger)ident{
    self = [super init];
    
    if (self){
        self.delegate = delegate;
        _ident = ident;
    }
    
    return self;
}

#pragma mark - Methods

-(void)makeRequestWithURL:(NSString *)url andParameters:(NSDictionary *)params andType:(enum CallType)type{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    NSString *encodedParams = [self EncodeUriWithParameters:params];
    
    if (type == GET){
        //BASEURI + url + "?" + encodedParams
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@?%@", BASEURL, url, encodedParams]]];
    } else if (type == POST){
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", BASEURL, url]]];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:[NSData dataWithBytes:[encodedParams UTF8String] length:strlen([encodedParams UTF8String])]];
    }
    //TODO: Realizar llamadas PUT / DELETE
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (connection)
        [connection start];
}

-(NSString*)EncodeUriWithParameters:(NSDictionary*)params{
    NSMutableString *result = [[NSMutableString alloc] init];

/*
 Los parámetros estarán codificados de la siguiente manera:
 param1=value1&param2=value2....
 */
    for (NSString *key in [params keyEnumerator]){
        if ([result length] > 0)
            [result appendString:@"&"];
        
        [result appendFormat:@"%@=%@", key, [params objectForKey:key]];
    }
    
    return result;
}

#pragma mark - NSURLConnection Delegates

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    if (_data == nil)
        _data = [[NSMutableData alloc] init];
    else
        [_data setLength:0];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [_data appendData:data];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    if (self.delegate != nil)
        [self.delegate ServiceCallErrorWithData:@"Error on service connection" andId:_ident];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if (self.delegate != nil)
        [self.delegate ServiceCallSuccededWithData:_data andId:_ident];
}

@end
