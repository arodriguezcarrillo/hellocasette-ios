//
//  AlbumsViewController.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 2/3/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Artist.h"
#import "MusicBO.h"

@interface AlbumsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MusicBODelegate>

-(void)setArtist:(Artist*)artist;


@end
