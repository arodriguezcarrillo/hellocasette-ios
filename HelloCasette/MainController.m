//
//  MainController.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 24/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "MainController.h"
#import "AlbumsViewController.h"

enum MainControllerZones{
    MainControllerMusicZone = 0,
    MainControllerUsersZone = 1,
    MainControllerCloseSession = 2
};

@interface MainController ()

@property (weak) IBOutlet UIView *menuView;
@property (weak) IBOutlet UIView *mainView;

@end

@implementation MainController{
    NSArray *menuData;
    BOOL menuOpened;
    MusicViewController *_musiccontroller;
    MapViewController *_mapcontroller;
    Artist *_openedArtist;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    menuData = @[
                    @"Música",
                    @"Cuenta",
                    @"Cerrar sesión"
        ];
    
    UIBarButtonItem *barLeft = [[self navigationItem] leftBarButtonItem];
    [barLeft setTarget:self];
    [barLeft setAction:@selector(menuClicked)];
}

-(void)menuClicked{
    [self openOrCloseMenuItem];
}
     
-(void)openOrCloseMenuItem{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    CGRect frame = self.menuView.frame;
    
    if (menuOpened == NO)
        frame.origin.x += 200;
    else
        frame.origin.x -= 200;
    
    self.menuView.frame = frame;
    
    [UIView commitAnimations];
    
    menuOpened = !menuOpened;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Music View Controller Delegate

-(void)ArtistClicked:(Artist *)artist{
    _openedArtist = artist;
    [self performSegueWithIdentifier:@"albumsSegue" sender:self];
}

#pragma mark - TableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [menuData count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MainCell" forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MainCell"];
    }
    
    UILabel *label = (UILabel*)[cell viewWithTag:0];
    [label setText:[menuData objectAtIndex:indexPath.row]];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIView *view;
    if (indexPath.row == MainControllerMusicZone){
        if (_musiccontroller == nil)
            _musiccontroller = [[MusicViewController alloc] initWithDelegate:self];
        
        view = _musiccontroller.view;
    } else if (indexPath.row == MainControllerUsersZone){
        if (_mapcontroller == nil)
            _mapcontroller = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
        
        view = _mapcontroller.view;
    }
    
    CGRect frame = view.frame;
    view.frame = frame;
    [self.mainView addSubview:view];
    
    [self openOrCloseMenuItem];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"albumsSegue"]){
        AlbumsViewController *viewController = (AlbumsViewController*)segue.destinationViewController;
        [viewController setArtist:_openedArtist];
    }
}

@end
