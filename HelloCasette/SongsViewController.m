//
//  SongsViewController.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 2/3/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "SongsViewController.h"
#import "Song.h"
#import <AVFoundation/AVFoundation.h>

@interface SongsViewController ()

@property (weak) IBOutlet UITableView *tableView;

@end

@implementation SongsViewController{
    NSArray *_songs;
    MusicBO *_businessObjects;
    Album *_album;
    NSInteger _selectedRow;
    
    AVPlayerItem *_playerItem;
    AVPlayer *_player;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _businessObjects = [[MusicBO alloc] initWithDelegate:self];
    
    [self LoadAlbumData];
}

-(void)LoadAlbumData{
    if (_album != nil){
        self.navigationItem.title = _album.name;
        [_businessObjects SongsByAlbum:_album.ident];
    }
}

-(void)setAlbum:(Album *)album{
    _album = album;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MusicBO Delegate

-(void)songsCompleted:(NSArray *)songs{
    _songs = songs;
    [self.tableView reloadData];
}

#pragma mark - TableView Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SongCell" forIndexPath:indexPath];
    
    UILabel *label = (UILabel*)[cell viewWithTag:0];
    
    Song *song = [_songs objectAtIndex:indexPath.row];
    [label setText:song.name];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_songs count];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedRow = indexPath.row;
    NSString *aSongURL = [[_songs objectAtIndex:_selectedRow] uri];
    [self PlaySongWithUri:aSongURL];
}


#pragma mark - Music player

-(void)PlaySongWithUri:(NSString *)uri{
    NSURL *url = [NSURL URLWithString:uri];
    _playerItem = [AVPlayerItem playerItemWithURL:url];
    _player = [AVPlayer playerWithPlayerItem:_playerItem];
    
    [_player play];
}


@end
