//
//  ServiceCall.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 25/2/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <Foundation/Foundation.h>

enum CallType{
    GET = 0,
    POST = 1,
    PUT = 2,
    DELETE = 3
};

@protocol ServiceCallDelegate

-(void)ServiceCallErrorWithData:(NSString*)error andId:(NSInteger)ident;
-(void)ServiceCallSuccededWithData:(NSData*)data andId:(NSInteger)ident;

@end

@interface ServiceCall : NSObject<NSURLConnectionDelegate, NSURLConnectionDataDelegate>

-(id)initWithDelegate:(id<ServiceCallDelegate>)delegate andId:(NSInteger)ident;
@property (weak) id<ServiceCallDelegate> delegate;
-(void)makeRequestWithURL:(NSString*)url andParameters:(NSDictionary*)params andType:(enum CallType)type;

@end
