//
//  SongsViewController.h
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 2/3/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Album.h"
#import "MusicBO.h"

@interface SongsViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, MusicBODelegate>

-(void)setAlbum:(Album*)album;

@end
