//
//  AlbumsViewController.m
//  HelloCasette
//
//  Created by Alberto Rodríguez Carrillo on 2/3/15.
//  Copyright (c) 2015 contoso. All rights reserved.
//

#import "AlbumsViewController.h"
#import "Album.h"
#import "SongsViewController.h"

@interface AlbumsViewController ()

@property (weak) IBOutlet UITableView *tableView;
@end

@implementation AlbumsViewController{
    NSArray *_albums;
    MusicBO *_businessObjects;
    Artist *_artist;
    NSInteger _selectedRow;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _businessObjects = [[MusicBO alloc] initWithDelegate:self];
    
    [self LoadArtistData];
}

-(void)LoadArtistData{
    if (_artist != nil){
        self.navigationItem.title = _artist.name;
        [_businessObjects AlbumsByArtist:_artist.ident];
    }
}

-(void)setArtist:(Artist *)artist{
    _artist = artist;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MusicBO Delegate

-(void)albumsCompleted:(NSArray *)albums{
    _albums = albums;
    [self.tableView reloadData];
}

#pragma mark - TableView Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AlbumCell" forIndexPath:indexPath];
    
    UILabel *label = (UILabel*)[cell viewWithTag:0];
    
    Album *album = [_albums objectAtIndex:indexPath.row];
    [label setText:album.name];
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_albums count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedRow = indexPath.row;
    [self performSegueWithIdentifier:@"songsSegue" sender:self];
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"songsSegue"]){
        SongsViewController *viewController = (SongsViewController*)segue.destinationViewController;
        
        [viewController setAlbum:[_albums objectAtIndex:_selectedRow]];
    }
}


@end
